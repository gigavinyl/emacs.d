;  __     __   __     __     ______   ______     __        
; /\ \   /\ "-.\ \   /\ \   /\__  _\ /\  ___\   /\ \       
; \ \ \  \ \ \-.  \  \ \ \  \/_/\ \/ \ \  __\   \ \ \____  
;  \ \_\  \ \_\\"\_\  \ \_\    \ \_\  \ \_____\  \ \_____\ 
;   \/_/   \/_/ \/_/   \/_/     \/_/   \/_____/   \/_____/ 


;;; Bad stuff
(setq package-check-signature nil)

;;; Making the interface not terrible ;;;
(menu-bar-mode -1)


;;; Sane defaults ;;;
(when (version<= "26.0.50" emacs-version)
  (setq display-line-numbers-type 'relative)
  (global-display-line-numbers-mode))
(setq server-use-tcp t)
(add-to-list 'exec-path "/home/gigavinyl/.nix-profile/bin/")
(global-set-key (kbd "<mouse-2>") 'clipboard-yank)
(setq x-select-enable-primary t)
(setq x-select-enable-clipboard t)
(setq-default word-wrap t)              ; wrap lines
(setq delete-old-versions -1)    ; delete excess backup versions silently
(setq version-control t)    ; use version control
(setq vc-make-backup-files t)    ; make backups file even when in version controlled dir
(setq backup-directory-alist `(("." . "~/.emacs.d/backups"))) ; which directory to put backups file
(setq vc-follow-symlinks t)               ; don't ask for confirmation when opening symlinked file
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t))) ;transform backups file name
(setq inhibit-startup-screen t)  ; inhibit useless and old-school startup screen
(setq ring-bell-function 'ignore)  ; silent bell when you make a mistake
(setq coding-system-for-read 'utf-8)  ; use utf-8 by default
(setq coding-system-for-write 'utf-8)
(set-language-environment "UTF-8")
(setq sentence-end-double-space nil)  ; sentence SHOULD end with only a point.
(setq default-fill-column 80)    ; toggle wrapping text at the 80th character
(setq-default indent-tabs-mode nil)     ; tabs to spaces
(setq tab-width 2)
(global-set-key "\C-m" 'newline-and-indent)
;; Scratch Buffer
;; (setq initial-scratch-message
;;       (format
;;        ";; %s\n\n"
;;        (replace-regexp-in-string
;;         "\n" "\n;; " ; comment each line
;;         (replace-regexp-in-string
;;          "\n$" ""    ; remove trailing linebreak
;;          (shell-command-to-string "toilet -d ~/labs/figlet-fonts -f Sub-Zero Im\ Gay")))))


;;; Setup Recent Files
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)


;;; Use package ;;;
(setq package-enable-at-startup nil) ; tells emacs not to load any packages before starting up
;; the following lines tell emacs where on the internet to look up
;; for new packages.
(setq package-archives '(("org"       . "http://orgmode.org/elpa/")
                         ("gnu"       . "http://elpa.gnu.org/packages/")
                         ("melpa"     . "https://melpa.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")))
(package-initialize) ; guess what this one does ?

                                        ; Bootstrap `use-package'
(unless (package-installed-p 'use-package) ; unless it is already installed
  (package-refresh-contents) ; updage packages archive
  (package-install 'use-package)) ; and install the most recent version of use-package

(require 'use-package) ; guess what this one does too ?
(setq use-package-always-ensure t) ; autodownload plugins
(use-package auto-package-update
  :config
  (setq auto-package-update-delete-old-versions t)
  (setq auto-package-update-hide-results t)
  (auto-package-update-maybe))

;;; Evil!!

                                        ; General
(use-package general
  :config
  (setq general-default-prefix "<SPC>")
  (general-evil-setup))

                                        ; Undo-tree
(use-package undo-tree)

                                        ;goto-chg
(use-package goto-chg)

                                        ; evil-mode
(use-package evil
  :init
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (setq evil-mode-line-format '(before . mode-line-front-space))
  (setq-default display-line-numbers 'visual
                display-line-numbers-widen t
                ;; this is the default
                display-line-numbers-current-absolute t)
  (defun noct:relative ()
    (setq-local display-line-numbers 'visual))
  (defun noct:absolute ()
    (setq-local display-line-numbers t))
  (add-hook 'evil-insert-state-entry-hook #'noct:absolute)
  (add-hook 'evil-insert-state-exit-hook #'noct:relative)
  (custom-set-faces '(line-number ((t :foreground "brightblack"))))
  (custom-set-faces '(line-number-current-line ((t :weight bold
                                                   :foreground "white")))))

                                        ; evil-escape
(use-package evil-escape
  :config
  (setq-default evil-escape-key-sequence "jk")
  (evil-escape-mode))

                                        ;  evil- commentary
(use-package evil-commentary
  :config
  (evil-commentary-mode))


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(line-number ((t :foreground "brightblack")))
 '(line-number-current-line ((t :weight bold :foreground "white"))))

;;; Editing Packages

                                        ;parinfer
(use-package parinfer
  :general
  (general-nmap
    "," 'parinfer-toggle-mode)
  :config
  (setq parinfer-extensions '(defaults pretty-parens smart-yank smart-tab paredit evil))
  :init
  (progn
    (setq parinfer-extensions
          '(defaults       ; should be included.
             pretty-parens  ; different paren styles for different modes.
             evil           ; If you use Evil.
             ;; lispy          ; If you use Lispy. With this extension, you should install Lispy and do not enable lispy-mode directly.
             paredit        ; Introduce some paredit commands.
             smart-tab      ; C-b & C-f jump positions and smart shift with tab & S-tab.
             smart-yank))   ; Yank behavior depend on mode.
    (add-hook 'clojure-mode-hook #'parinfer-mode)
    (add-hook 'emacs-lisp-mode-hook #'parinfer-mode)
    (add-hook 'common-lisp-mode-hook #'parinfer-mode)
    (add-hook 'scheme-mode-hook #'parinfer-mode)
    (add-hook 'lisp-mode-hook #'parinfer-mode)))

                                        ; paredit
(use-package paredit
  :config
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
  (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
  (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
  (add-hook 'clojure-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-mode-hook             #'enable-paredit-ode)
  (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
  (add-hook 'scheme-mode-hook           #'enable-paredit-mode))

                                        ;yasnippet
(use-package yasnippet
  :config
  (yas-global-mode 1))



                                        ; focus-mode
(use-package focus
  :commands focus-mode)

(use-package writeroom-mode
  :commands writeroom-mode)

                                        ; flyspell

(use-package flyspell
  :after (:any tex org)
  :config
  (setq ispell-program-name "aspell")
  ;; Please note ispell-extra-args contains ACTUAL parameters passed to aspell
  (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US")))

                                        ; company-mode
(use-package company
  :config
  (add-hook 'after-init-hook 'global-company-mode))
(use-package pos-tip
  :after compmay)
(use-package company-quickhelp
  :after company
  :config (company-quickhelp-mode))

                                        ;flycheck
(use-package flycheck
  :config
  (global-flycheck-mode))

(use-package flycheck-pos-tip
  :after flycheck
  :config
  (with-eval-after-load 'flycheck
    (flycheck-pos-tip-mode)))
                                        ; lsp
(use-package lsp-mode
  :hook (XXX-mode . lsp)
  :commands lsp)

;; optionally
(use-package lsp-ui 
  :commands lsp-ui-mode
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))
(use-package company-lsp :commands company-lsp)
                                        ;(use-package helm-lsp :commands helm-lsp-workspace-symbol)
                                        ;(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;;; Evil!!

                                        ; General
(use-package general
  :config
  (setq general-default-prefix "<SPC>")
  (general-evil-setup))

                                        ; Undo-tree
(use-package undo-tree)

                                        ;goto-chg
(use-package goto-chg)

                                        ; evil-mode
(use-package evil
  :init
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (setq evil-mode-line-format '(before . mode-line-front-space))
  (setq-default display-line-numbers 'visual
                display-line-numbers-widen t
                ;; this is the default
                display-line-numbers-current-absolute t)
  (defun noct:relative ()
    (setq-local display-line-numbers 'visual))
  (defun noct:absolute ()
    (setq-local display-line-numbers t))
  (add-hook 'evil-insert-state-entry-hook #'noct:absolute)
  (add-hook 'evil-insert-state-exit-hook #'noct:relative)
  (custom-set-faces '(line-number ((t :foreground "brightblack"))))
  (custom-set-faces '(line-number-current-line ((t :weight bold
                                                   :foreground "white")))))

                                        ; evil-escape
(use-package evil-escape
  :config
  (setq-default evil-escape-key-sequence "jk")
  (evil-escape-mode))

                                        ;  evil- commentary
(use-package evil-commentary
  :config
  (evil-commentary-mode))


;;; Interface

                                        ;theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'xresources t)

(set-default-font "scientifica 12")

                                        ; fix those pesky highligts
(set-face-attribute 'region nil :background "brightblack")

                                        ;all-the-icons
(use-package all-the-icons)

                                        ;neo-tree
(use-package neotree
  :config
  (setq neo-theme (if (display-graphic-p) 'icons 'arrow))
  (global-set-key [f8] 'neotree-toggle))
                                        ;which-key
(use-package which-key
  :config
  (which-key-mode)  
  (setq which-key-allow-evil-operators t))

                                        ;rainbow-delimiters
(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

                                        ; rich-minority
(use-package rich-minority
  :config
  (setq rm-whitelist "*")
  (rich-minority-mode 1))

                                        ; smart-mode-line
(use-package smart-mode-line
  :config
  (setq sml/theme 'respectful)
  (setq sml/no-confirm-load-theme t)
  (sml/setup))

;; ivy

                                        ;flx
(use-package flx)

                                        ;ivy
(use-package ivy
  :config
  (setq ivy-initial-inputs-alist nil)
  ;; (setq ivy-re-builders-alist
  ;;       '((ivy-switch-buffer . ivy--regex-plus
  ;;                            (t . ivy--regex-fuzzy))))
  (ivy-mode 1))

                                        ;swiper
(use-package swiper
  :after ivy)

                                        ;counsel
(use-package counsel
  :after ivy
  :general
  (general-nmap
    ":" 'counsel-M-x))

                                        ; Parens
(show-paren-mode 1)
(set-face-background 'show-paren-match "brightblack"
                     (set-face-attribute 'show-paren-match nil :weight 'bold))
(use-package smartparens
  :config
  (require 'smartparens-config)
  (smartparens-global-mode 1))


;;; Org Mode

                                        ; org
(use-package org
  :mode ("\\.org\\'" . org-mode)
  :interpreter ("org" . org-mode)
  :config
  (custom-set-variables)
  '(org-directory "~/.org")
  '(org-agenda-files (list org-directory)))

                                        ; evil-org
(use-package evil-org
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme '(textobjects insert navigation additional shift todo heading))))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

                                        ;org-journal ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
(use-package org-journal
  :custom
  (org-journal-dir "~/org/journal/")
  (org-journal-date-format "%A, %d %B %Y"))

;;; LaTeX

(use-package tex
  :ensure auctex
  :mode ("\\.tex\\'" . LaTeX-mode)
  :interpreter ("tex" . LaTeX-mode)
  :config
  (setq TeX-save-query nil)
  (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)
  (setq TeX-source-correlate-mode t)
  (setq LaTeX-item-indent 1)
  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq-default TeX-master nil)
  (add-hook 'LaTeX-mode-hook 'visual-line-mode)
  (add-hook 'LaTeX-mode-hook 'flyspell-mode)
  (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
  (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
  (add-hook 'LaTeX-mode-hook 'TeX-interactive-mode)
  (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
  (setq TeX-view-program-selection '((output-pdf "Zathura")))
  (eval-after-load "tex"
    '(progn
       (add-to-list
        'TeX-engine-alist
        '(default-shell-escape "Default with shell escape"
           "pdftex -shell-escape"
           "pdflatex -shell-escape"
           ConTeXt-engine))))
  ;; (setq-default TeX-engine 'default-shell-escape)
  
  (setq reftex-plug-into-AUCTeX t))

(use-package company-auctex
  :after tex
  :config
  (company-auctex-init))

(use-package company-math
  :after tex
  :config
  (defun my-latex-mode-setup ()
    (setq-local company-backends
                (append '((company-math-symbols-latex company-math-symbols-unicode company-latex-commands company-auctex))
                        company-backends)))
  (add-hook 'TeX-mode-hook 'my-latex-mode-setup))

;;; projects

                                        ; projectile                            ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
(use-package projectile
  :general
  (general-nmap
    "p" '(:keymap projectile-command-map :wk "projectile prefix")))

                                        ; magit                                 ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
(use-package magit)

(use-package evil-magit)


;;; Misc

(use-package w3m)

(use-package circe
  :config 
  (setq auth-sources '("~/.authinfo.gpg"))

  (defun my-fetch-password (&rest params)
    (require 'auth-source)
    (let ((match (car (apply 'auth-source-search params))))
      (if match
          (let ((secret (plist-get match :secret)))
            (if (functionp secret)
                (funcall secret)
              secret))
        (error "Password not found for %S" params))))

  (defun my-nickserv-password (server)
    (my-fetch-password :login "forcer" :machine "irc.freenode.net"))

  (setq circe-network-options
        '(("Freenode"
           :nick "gigavinyl"
           :channels ("#emacs" "#emacs-circe" "#clojure" "#netbsd")
           :nickserv-password my-nickserv-password))))

(use-package exec-path-from-shell
  :config
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)))

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package editorconfig
  :config
  (editorconfig-mode 1))

(use-package aggressive-indent
  :config
  (global-aggressive-indent-mode 1))

                                        ;(add-to-list 'load-path "~/.emacs.d/scel/el") ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;
                                        ;(require 'sclang) ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ; ;

;;; Rust
(use-package rust-mode
  :mode ("\\.rs\\'" . rust-mode)
  :interpreter ("rust" . rust-mode))

(use-package flycheck-rust
  :after rust-mode)

(use-package racer
  :after rust-mode
  :config
  (add-hook 'rust-mode-hook #'racer-mode)
  (add-hook 'racer-mode-hook #'eldoc-mode)
  (add-hook 'racer-mode-hook #'company-mode)
  (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
  (setq company-tooltip-align-annotations t))

(use-package toml-mode
  :mode ("\\.toml\\'" . toml-mode)
  :interpreter ("rust" . toml-mode))

;;; Haskell
(use-package haskell-mode
  :mode ("\\.hs\\'" . haskell-mode)
  :interpreter ("haskell" . haskell-mode))

(use-package lsp-haskell
  :config
  (add-hook 'haskell-mode-hook #'lsp))


;;; Clojure
(use-package clojure-mode
  :mode ("\\.clj*\\'" . clojure-mode)
  :interpreter ("clojure" . clojure-mode))

(use-package cider
  :after clojure-mode
  :config
  (add-hook 'clojure-mode-hook #'cider-mode)
  (setq nrepl-hide-special-buffers t))

(use-package emidje
  :after clojure-mode
  :config
  (eval-after-load 'cider
    #'emidje-setup))

(use-package flycheck-clojure
  :after flycheck
  :config
  (eval-after-load 'flycheck '(flycheck-clojure-setup))
  (add-hook 'after-init-hook #'global-flycheck-mode))

;; (use-package clj-refactor
;;   :after clojure-mode
;;   :config
;;   (defun my-clojure-mode-hook ()
;;     (clj-refactor-mode 1
;;                        (yas-minor-mode 1))
;;     (cljr-add-keybindings-with-prefix "C-c C-m"))
;;   (add-hook 'clojure-mode-hook #'my-clojure-mode-hook)) ; for adding require/use/import statements
;; This choice of keybinding leaves cider-macroexpand-1 unbound

;;; Misc langs
(use-package yaml-mode
  :mode ("\\.yaml\\'" . yaml-mode)
  :interpreter ("yaml" . yaml-mode))


(general-define-key
 :states 'normal
 "rl" 'load-file "~/emacs.d/init.el")

(defun goto-match-paren (arg)
  "Go to the matching parenthesis if on parenthesis, otherwise insert %.)
  vi style of % jumping to matching brace."
  (interactive "p")
  (cond ((looking-at "\\s\(\") (forward-list 1) (backward-char 1)
((looking-at \"\\s\)\") (forward-char 1) (backward-list 1))
(t (self-insert-command (or arg 1)))))\)"))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "e4c8810d9ab925567a69c11d5c95d198a4e7d05871453b2c92c020712559c4c1" default)))
 '(org-journal-date-format "%A, %d %B %Y")
 '(org-journal-dir "~/org/journal/")
 '(package-selected-packages
   (quote
    (clj-refactor paredit yascroll yaml-mode writeroom-mode which-key w3m use-package toml-mode smartparens smart-mode-line rainbow-delimiters racer projectile parinfer org-plus-contrib org-journal neotree multiple-cursors minimap lsp-ui lsp-haskell inflections hydra general focus flycheck-rust flycheck-pos-tip flycheck-clojure flx exec-path-from-shell evil-org evil-magit evil-escape evil-commentary emidje edn editorconfig counsel company-quickhelp company-math company-lsp company-auctex bug-hunter auto-package-update all-the-icons aggressive-indent))))
